import json,re

NAME_PRINCIPAL = 'Datos principales'
NAME_SECUNDARIO = 'Revisión técnico mecánica'
NAME_TERCERO = 'Revisión técnico Ambiental'
XPATH_SECUNDARIO = "//*[@id='pnlRevisionTecnicoMecanicaNacional']"
XPATH_TERCERO = "//*[@id='pnlRevisionTecnicoMecanicaNacionalOtras']"

def save_json(dict_file):
    path_local = open("config.json","w")
    json.dump(dict_file,path_local)
    path_local.close()
    
def open_json()-> dict:
    files = open("config.json")
    config = json.load(files)
    files.close()
    return config

def reemplazar_nombre(value:str):
    try:
        ver_value = re.search("(?<=\.)\w+",value.strip()).group(0)
        if ver_value not in ["xlsx","xls","xlsm"] : return ""
        return value.strip()
    except Exception as _:
        return f"{value.strip()}.xlsx"