

import os
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import pandas as pd
from Scrapping import main
from Util import open_json, save_json

from config_view import config_view

class Main:

    def __init__(self):
        self.read_file = None
        self.bool_file = False
        self.root= tk.Tk()
        self.root.title("Scrapping RTM")
        self.root.geometry('350x300+500+150')
        self.root.resizable(False, False)
        self.canvas1 = tk.Canvas(self.root, width = 350, height = 300, bg = 'lightsteelblue2', relief = 'raised')
        self.canvas1.pack()
    
        label1 = tk.Label(self.root, text='Scrapping RTM', bg = 'lightsteelblue2')
        label1.config(font=('helvetica', 20))
        self.canvas1.create_window(180, 50, window=label1)
        
        self.init_last()
        self.getExcelButton()
        self.scrapping_button()
        self.config_app_button()
        self.exitButton()
        
        
        self.root.mainloop()
        
    def init_last(self):
        if "config.json" not in os.listdir():
            save_json({"last_rtm": "", "file_scraping": "", "excel_path": "", "filename": ""})
        else:
            config = open_json()
            if config["file_scraping"] != "" and config["last_rtm"] != "":
                messagebox.showinfo(parent=self.root, message=f"Por favor inicie Scraping e iniciará desde la última linea que quedó con el archivo {config['file_scraping']} \nen el NUM_rtm = {config['last_rtm']}" )
                self.read_file = pd.read_excel(config["file_scraping"],engine='openpyxl').drop_duplicates(subset='Nro_RTM')
                self.bool_file = True
        
    def getExcelButton(self):
        
        browseButton_Excel = tk.Button(text="      Importar archivo Excel     ", command=self.getExcel, bg='green', fg='white', font=('helvetica', 12, 'bold'))
        self.canvas1.create_window(180, 120, window=browseButton_Excel)
    
    def getExcel(self):
        if self.config_app(): return
        try:
            import_file_path = filedialog.askopenfilename(title = "Seleccione archivo",filetypes=[("excel files", ".xls .xlsm .xlsx")])
            if (len(import_file_path) != 0):
                config = open_json()
                config["file_scraping"] = import_file_path
                save_json(config)
                self.read_file = pd.read_excel(import_file_path,engine='openpyxl').drop_duplicates(subset='Nro_RTM')
                self.bool_file = True
        except Exception as e:
            messagebox.showerror(self.root,e)
            
    def scrapping_button(self):
        browseButton_Excel = tk.Button(text="      Hacer el Scrapping del Excel     ", command=self.do_scrapping, bg='blue', fg='white', font=('helvetica', 12, 'bold'))
        self.canvas1.create_window(180, 170, window=browseButton_Excel)
        
    def do_scrapping(self):
        if not self.bool_file:
            messagebox.showwarning(self.root,"Por favor seleccione el Excel primero")
        elif ("Nro_RTM" in self.read_file.columns):
            try:
                main(self.read_file)
            except FileNotFoundError as _:
                messagebox.showerror(parent=self.root,title="Error de lectura",message="Por favor revise la ruta que ingresó")      
            # except Exception as ex:
            #     template = "Un error de tipo {0} ocurrido. Argumentos:\n{1!r}"
            #     message = template.format(type(ex).__name__, ex.args)
            #     messagebox.showerror(self.root,message)
            #     messagebox.showinfo(self.root, "Por favor inicie nuevamente el Scraping e iniciará desde la última linea que quedó")
        else:
            messagebox.showwarning(self.root,"Seleccion el Excel correcto (Debe tener un solo campo con el nombre 'Nro_RTM'")
            
    def config_app_button(self):
        browseButton_Excel = tk.Button(text="      Configuración     ", command=config_view, bg='yellow', fg='black', font=('helvetica', 12, 'bold'))
        self.canvas1.create_window(180, 220, window=browseButton_Excel)
        
    
    def config_app(self):
        config = open_json()
        if config["excel_path"] == "" or config["filename"] == "":
            config_view()
            return True
        return False
            
    
    def exitButton(self):
        exitButton = tk.Button (self.root, text='       Exit Application     ',command=self.exitApplication, bg='brown', fg='white', font=('helvetica', 12, 'bold'))
        self.canvas1.create_window(180, 270, window=exitButton)
        
    
    def exitApplication(self):
        MsgBox = tk.messagebox.askquestion ('Exit Application','Estás seguro que quieres salir de la APP',icon = 'warning')
        if MsgBox == 'yes':
            self.root.destroy()
            
    
Main()



    


     

