import os,time,re,sys,datetime
from bs4 import BeautifulSoup
from bs4.element import ResultSet
from selenium import webdriver
import pandas as pd
from selenium.common.exceptions import ElementNotInteractableException, NoSuchElementException, TimeoutException, WebDriverException
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from Util import XPATH_SECUNDARIO, XPATH_TERCERO, open_json, save_json,NAME_PRINCIPAL,NAME_SECUNDARIO,NAME_TERCERO
from read_captcha import get_text_image



def ingresar():
    global driver
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--log-level=3")

    if (sys.platform == 'linux'):
        driver = webdriver.Chrome(ChromeDriverManager().install(),chrome_options=chrome_options)
    elif (sys.platform == 'win32'):
        driver = webdriver.Chrome(ChromeDriverManager().install(),chrome_options=chrome_options)
    else:
        return "No hay driver para esta plataforma"



def asignar_valores(df: pd.DataFrame):
    ingresar()
    try:
        datosPrincipal = pd.read_excel(os.path.join(conf["excel_path"],conf["filename"]),
                                sheet_name=NAME_PRINCIPAL)
        datosSecundarios = pd.read_excel(os.path.join(conf["excel_path"],conf["filename"]),
                               sheet_name=NAME_SECUNDARIO)
        datosTerceros = pd.read_excel(os.path.join(conf["excel_path"],conf["filename"]),
                               sheet_name=NAME_TERCERO)
    except FileNotFoundError as _:
        datosPrincipal =   pd.DataFrame(columns=["Nro_RTM","PLACA_VEHÍCULO","NRO_LICENCIA_TRÁNSITO","ESTADO_VEHÍCULO","TIPO_SERVICIO","CLASE_VEHÍCULO"])
        datosSecundarios = pd.DataFrame(columns=["Nro_RTM","Tipo Revisión",	"Fecha Expedición","Fecha Vigencia","CDA expide RTM","Vigente",	"Nro. certificado",	"Información consistente"])
        datosTerceros =    pd.DataFrame(columns=["Nro_RTM","Tipo Revisión","Fecha Expedición","Fecha Vigencia","CDA expide RTM","Vigente","Nro. certificado","Información consistente"])
    for i, row in df.iterrows():
        if conf["last_rtm"] != "" and conf["last_rtm"] != row["Nro_RTM"]: continue
        
        try:
            while True:
                try:
                    driver.get("https://www.runt.com.co/consultaCiudadana/#/consultaVehiculo")
                except (TimeoutException,WebDriverException) as e:
                    print(e)
                WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//select[@class='form-control ng-pristine ng-valid']/option[text()='RTM']"))).click()
                rtm: WebElement = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                    (By.ID, "rtm")))
                rtm.send_keys(row["Nro_RTM"])
                time.sleep(2)
                img_catpcha: WebElement = WebDriverWait(driver, 5).until(EC.presence_of_element_located(
                    (By.ID, "imgCaptcha")))
                open("captcha4.png", "wb").write(img_catpcha.screenshot_as_png)
                txt_catpcha = get_text_image()

                catpcha: WebElement = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                    (By.ID, "captchatxt")))
                catpcha.send_keys(txt_catpcha)
                time.sleep(1)
                driver.execute_script("arguments[0].click();",WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div[1]/div/div[1]/div[2]/div[1]/div[2]/div[1]/div[3]/div[2]/div/div/form/div[9]/button"))))
                time.sleep(3)
                try:
                    msg:WebElement = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                            (By.ID, "msgConsulta")))
                    if msg.text in ["Señor Usuario, para el vehículo consultado no hay información registrada en el sistema RUNT.","No se encontró información del vehículo"]:
                        print(msg.text)
                        break

                except (TimeoutException) as _:
                    pass
                try:
                    WebDriverWait(driver, 10).\
                        until(EC.presence_of_element_located(
                            (By.XPATH, "//div[@id='dlgConsulta']/div/div/div[2]/div/button"))).click()
                    continue
                except (TimeoutException,NoSuchElementException,ElementNotInteractableException) as _:
                    break
            
            time.sleep(1)
            desplegar_datos()
            time.sleep(1)
            datosPrincipal = pd.concat([datosPrincipal,get_datos(row["Nro_RTM"],
                                                datosPrincipal.columns)])
            
            datosSecundarios= pd.concat([datosSecundarios,get_datos_tabla(row["Nro_RTM"],
                                                datosSecundarios.columns,XPATH_SECUNDARIO)])
           
            datosTerceros= pd.concat([datosSecundarios,get_datos_tabla(row["Nro_RTM"],
                                                datosSecundarios.columns,XPATH_TERCERO)])
            guardar_datos(datosPrincipal,datosSecundarios,datosTerceros)
            print(f"RTM num {i+1} guardado: {row['Nro_RTM']} a las {datetime.datetime.now()}")
            conf["last_rtm"] = ""
        except Exception as e:
            print("Guarda conf.json")
            conf["last_rtm"] = row["Nro_RTM"]
            save_json(conf)
            driver.close()
            raise e
            
def guardar_datos(prin:pd.DataFrame,sec:pd.DataFrame,ter:pd.DataFrame):
    
    writer = pd.ExcelWriter(os.path.join(conf["excel_path"],conf["filename"]), engine='openpyxl') 
    prin.to_excel(writer, sheet_name='Datos principales',index=False,encoding='utf-8')
    sec.to_excel(writer, sheet_name='Revisión técnico mecánica',index=False)
    ter.to_excel(writer, sheet_name='Revisión técnico Ambiental',index=False)

    writer.save()
    writer.close()
    
    

def desplegar_datos():
    try:
        for val in WebDriverWait(driver, 10).until(EC.presence_of_all_elements_located(
                    (By.XPATH, "//i[@class='i_hoja s_25_o1 ib']"))):
            driver.execute_script("arguments[0].click();",val)
    except TimeoutException as _:
        pass

def get_datos(n_rtm:str,columns:list):
    try:
        datos_principales:WebElement = WebDriverWait(driver, 5).until(EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div[1]/div/div[1]/div[2]/div[1]/div[2]/div[1]/div[5]/div[2]")))
        soup = BeautifulSoup(datos_principales.get_attribute('innerHTML'),'html5lib')
        elements:ResultSet = soup.find_all("div",{"class":re.compile("show-grande")})
        df = pd.DataFrame([[n_rtm]+[str(el.text).strip().replace("\n","") for el in elements]], columns=columns)
        return df
    except TimeoutException as _:
        return pd.DataFrame([[n_rtm]+[None]*(len(columns)-1)], columns=columns)
        

def get_datos_tabla(n_rtm,columns,xpath:str):
    try:
        segundos_datos:WebElement = WebDriverWait(driver, 5).until(EC.presence_of_element_located(
                    (By.XPATH, xpath)))
        soup = BeautifulSoup(segundos_datos.get_attribute('innerHTML'),'html5lib')
    except TimeoutException as _:
        return pd.DataFrame([[n_rtm]+[None]*(len(columns)-1)],columns=columns)
    try:
        df = pd.read_html(str(soup))[0]
        if "Acciones" in df.columns:
            serie = pd.DataFrame([[n_rtm]+df.iloc[0,:-1].to_list()],columns=columns)
        else:
            serie = pd.DataFrame([[n_rtm]+df.values.tolist()[0]],columns=columns)
    except IndexError as _:
        serie = pd.DataFrame([[n_rtm]+[None]*(len(columns)-1)],columns=columns)
    return serie
    
def main(df: pd.DataFrame):
    global conf
    conf = open_json()
    asignar_valores(df.astype(str))
    conf["file_scraping"] = ""
    save_json(conf)
    driver.close()
    print("Scraping terminado")
