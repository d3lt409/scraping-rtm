import sys
import cv2
import pytesseract
import os
import numpy as np

def quitar_sombra(img):
    rgb_planes = cv2.split(img)

    result_planes = []
    result_norm_planes = []
    for plane in rgb_planes:
        dilated_img = cv2.dilate(plane, np.ones((7,7), np.uint8))
        bg_img = cv2.medianBlur(dilated_img, 21)
        diff_img = 255 - cv2.absdiff(plane, bg_img)
        norm_img = cv2.normalize(diff_img,None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        result_planes.append(diff_img)
        result_norm_planes.append(norm_img)

    result = cv2.merge(result_planes)

    cv2.imwrite('captcha4.png', result)

def qutar_background(frame):
    
    fgbg = cv2.createBackgroundSubtractorMOG2()
    fgmask = fgbg.apply(frame)
    
    cv2.imwrite('captcha4.png',fgmask)

def quitar_lineas():
    image = cv2.imread('captcha4.png')
    image = cv2.blur(image, (3, 3))
    ret, image = cv2.threshold(image, 90, 255, cv2.THRESH_BINARY)

    image = cv2.dilate(image, np.ones((3, 1), np.uint8))
    image = cv2.erode(image, np.ones((2, 2), np.uint8))

    cv2.imwrite('captcha4.png',image)

def get_text_image():
    if sys.platform == 'win32':
        pytesseract.pytesseract.tesseract_cmd = 'C:\Program Files\Tesseract-OCR\\tesseract.exe'
    #os.system("convert captcha4.jpeg -colorspace gray captcha4.jpeg")
    try:
        newImg = cv2.imread("captcha4.png")
        newImg = newImg[10:50,10:140]
        cv2.imwrite('captcha4.png',newImg)
        os.system("convert captcha4.png -negate -morphology erode octagon:2 -negate captcha4.png")
        quitar_sombra(cv2.imread("captcha4.png"))
    except Exception as _:
        return ""
    
    # os.system("convert captcha4.png -colorspace gray captcha4.png")
    os.system("convert captcha4.png -level 40%,100% captcha4.png")
    #os.system("convert captcha4.png -gaussian-blur 0 captcha4.png")
    #os.system("convert captcha4.png -threshold 60% captcha4.png")
    captcha:str = pytesseract.image_to_string(cv2.imread("captcha4.png"),
                                config='--psm 8 -c tessedit_char_whitelist=8f2345678abcdefghkmnprwxy')
    captcha = captcha.replace(" ", "").strip()
    return captcha
